<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title></title>
	<!-- Bootswatch -->
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/solar/bootstrap.min.css">
</head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark ">
			<a class="navbar-brand" href="../index.php">Jeriam's Restaurant</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>

			<div class="collapse navbar-collapse" id="navbarColor02">
				<ul class="navbar-nav mr-auto ">
					<li class="nav-item active">
						<a class="nav-link" href="catalog.php">Bar Menu <span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="add-item.php">Add Item</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="cart.php">Cart</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="#">About</a>
					</li>
				</ul>
			</div>
		</nav>

		<!-- Page Contents -->
		<?php get_body_contents()

		 ?>

		<!-- Footer -->
		<footer class="page-footer font-small navbar-dark bg-dark ">
			<div class="footer-copyright text-center py-3">2020 Made with Love by:Joey Dela Cruz Bonina</div>
		</footer>
</body>
</html>